# Using Owl Mailman with docker

## Running bash

```bash
docker run -ti --rm --name owl-mailman-cli -v $PWD:/app piotrekr/php-composer:7.1 bash
```

## Running CLI from bash

```bash
bin/cli.php
```
