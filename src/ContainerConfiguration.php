<?php
declare(strict_types=1);

namespace OwlLabs\OwlMailman\Cli;

use Aura\Di\Container;
use Aura\Di\ContainerConfigInterface;

/**
 * Class ContainerConfiguration
 * @package OwlLabs\OwlMailman\Cli
 */
class ContainerConfiguration implements ContainerConfigInterface
{
    /**
     * @var array
     */
    private $config;

    /**
     * ContainerConfiguration constructor.
     * @param array $config
     */
    public function __construct(array $config)
    {
        $this->config = $config;
    }

    /**
     * Define params, setters, and services before the Container is locked.
     * @param Container $di The DI container.
     * @return void
     */
    public function define(Container $di)
    {
        $di->set('config', new \ArrayObject($this->config, \ArrayObject::ARRAY_AS_PROPS));

        if (empty($this->config['dependencies'])) {
            return;
        }

        $dependencies = $this->config['dependencies'];

        // Inject delegator factories
        // This is done early because Aura.Di does not allow modification of a
        // service after creation. As such, we need to create custom factories
        // for each service with delegators.
//        if (isset($dependencies['delegators'])) {
//            $dependencies = $this->marshalDelegators($di, $dependencies);
//        }

        // Inject services
        if (isset($dependencies['services'])) {
            foreach ($dependencies['services'] as $name => $service) {
                $di->set($name, $service);
            }
        }

        // Inject factories
        if (isset($dependencies['factories'])) {
            foreach ($dependencies['factories'] as $service => $factory) {
                $di->set($factory, $di->lazyNew($factory));
                $di->set($service, $di->lazyGetCall($factory, '__invoke', $di));
            }
        }

        // Inject invokables
        if (isset($dependencies['invokables'])) {
            foreach ($dependencies['invokables'] as $service => $class) {
                $di->set($service, $di->lazyNew($class));
            }
        }

        // Inject aliases
        if (isset($dependencies['aliases'])) {
            foreach ($dependencies['aliases'] as $alias => $target) {
                $di->set($alias, $di->lazyGet($target));
            }
        }
    }

    /**
     * Modify service objects after the Container is locked.
     * @param Container $di The DI container.
     * @return void
     */
    public function modify(Container $di)
    {
    }
}
