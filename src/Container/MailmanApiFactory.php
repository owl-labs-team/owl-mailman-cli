<?php
declare(strict_types=1);

namespace OwlLabs\OwlMailman\Cli\Container;

use OwlLabs\OwlMailman\Client\Configuration;
use OwlLabs\OwlMailman\Client\MailmanApi;
use Psr\Container\ContainerInterface;

/**
 * Class MailmanApiFactory
 * @package OwlLabs\OwlMailman\Cli\Container
 */
class MailmanApiFactory
{
    /**
     * @param ContainerInterface $container
     * @return MailmanApi
     */
    public function __invoke(ContainerInterface $container): MailmanApi
    {
        $configData = $container->get('config')[MailmanApi::class];
        $configuration = new Configuration($configData['username'], $configData['password'], $configData['uriPrefix']);
        return new MailmanApi($configuration);
    }
}
