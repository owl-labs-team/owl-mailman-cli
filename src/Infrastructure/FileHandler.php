<?php
declare(strict_types=1);

namespace OwlLabs\OwlMailman\Cli\Infrastructure;

/**
 * Class FileHandler
 * @package OwlLabs\OwlMailman\Cli\Infrastructure
 */
class FileHandler
{
    /**
     * @param string $filepath
     * @return string
     */
    public function loadContents(string $filepath): string
    {
        if (!is_readable($filepath)) {
            throw new FileHandlerException('File does not exist or is not readable');
        }
        return file_get_contents($filepath);
    }
}
