<?php
declare(strict_types=1);

namespace OwlLabs\OwlMailman\Cli\Infrastructure;

/**
 * Class FileHandlerException
 * @package OwlLabs\OwlMailman\Cli\Infrastructure
 */
class FileHandlerException extends \RuntimeException
{
}
