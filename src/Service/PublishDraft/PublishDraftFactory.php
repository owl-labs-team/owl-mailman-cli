<?php
declare(strict_types=1);

namespace OwlLabs\OwlMailman\Cli\Service\PublishDraft;

use OwlLabs\OwlMailman\Client\MailmanApi;
use Psr\Container\ContainerInterface;

/**
 * Class PublishDraftFactory
 * @package OwlLabs\OwlMailman\Cli\Service\PublishDraft
 */
class PublishDraftFactory
{
    /**
     * @param ContainerInterface $container
     * @return PublishDraftService
     */
    public function __invoke(ContainerInterface $container): PublishDraftService
    {
        return new PublishDraftService(
            $container->get(MailmanApi::class)
        );
    }
}
