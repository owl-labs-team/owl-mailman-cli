<?php
declare(strict_types=1);

namespace OwlLabs\OwlMailman\Cli\Service\PublishDraft;

use OwlLabs\OwlMailman\Client\Data\Input\TemplateVersionModify;
use OwlLabs\OwlMailman\Client\MailmanApi;

/**
 * Class PublishDraftService
 * @package OwlLabs\OwlMailman\Cli\Service\PublishDraft
 */
class PublishDraftService
{
    /**
     * @var MailmanApi
     */
    private $api;

    /**
     * PublishDraftService constructor.
     * @param MailmanApi $api
     */
    public function __construct(MailmanApi $api)
    {
        $this->api = $api;
    }

    /**
     * @param string $templateId
     * @return void
     */
    public function publish(string $templateId)
    {
        $versionView = $this->api->template($templateId)->version('draft')->read();

        $input = new TemplateVersionModify($versionView->contents());

        $this->api->template($templateId)->version('public')->modify($input);
    }
}
