<?php
declare(strict_types=1);

namespace OwlLabs\OwlMailman\Cli\Service\PreviewTemplate;

use OwlLabs\OwlMailman\Client\MailmanApi;
use Psr\Container\ContainerInterface;

/**
 * Class PreviewTemplateFactory
 * @package OwlLabs\OwlMailman\Cli\Service\PreviewTemplate
 */
class PreviewTemplateFactory
{
    /**
     * @param ContainerInterface $container
     * @return PreviewTemplateService
     */
    public function __invoke(ContainerInterface $container): PreviewTemplateService
    {
        return new PreviewTemplateService(
            $container->get(MailmanApi::class)
        );
    }
}
