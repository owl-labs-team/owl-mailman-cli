<?php
declare(strict_types=1);

namespace OwlLabs\OwlMailman\Cli\Service\PreviewTemplate;

use OwlLabs\OwlMailman\Client\Data\Parameters\TemplateVersionPreview;
use OwlLabs\OwlMailman\Client\MailmanApi;

/**
 * Class PreviewTemplateService
 * @package OwlLabs\OwlMailman\Cli\Service\PreviewTemplate
 */
class PreviewTemplateService
{
    /**
     * @var MailmanApi
     */
    private $api;

    /**
     * PreviewTemplateService constructor.
     * @param MailmanApi $api
     */
    public function __construct(MailmanApi $api)
    {
        $this->api = $api;
    }

    /**
     * @param string $templateId
     * @param array $variables
     * @return string
     */
    public function preview(string $templateId, string $versionId, array $variables): string
    {
        $parameters = new TemplateVersionPreview($variables);
        $preview = $this->api->template($templateId)->version($versionId)->preview($parameters);
        return $preview->html();
    }
}
