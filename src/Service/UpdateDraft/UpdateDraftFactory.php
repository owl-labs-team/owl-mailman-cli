<?php
declare(strict_types=1);

namespace OwlLabs\OwlMailman\Cli\Service\UpdateDraft;

use OwlLabs\OwlMailman\Cli\Infrastructure\FileHandler;
use OwlLabs\OwlMailman\Client\MailmanApi;
use Psr\Container\ContainerInterface;

/**
 * Class UpdateDraftFactory
 * @package OwlLabs\OwlMailman\Cli\Service\UpdateDraft
 */
class UpdateDraftFactory
{
    /**
     * @param ContainerInterface $container
     * @return UpdateDraftService
     */
    public function __invoke(ContainerInterface $container): UpdateDraftService
    {
        return new UpdateDraftService(
            $container->get(FileHandler::class),
            $container->get(MailmanApi::class)
        );
    }
}
