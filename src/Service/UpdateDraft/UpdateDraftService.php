<?php
declare(strict_types=1);

namespace OwlLabs\OwlMailman\Cli\Service\UpdateDraft;

use OwlLabs\OwlMailman\Cli\Infrastructure\FileHandler;
use OwlLabs\OwlMailman\Client\Data\Input\TemplateVersionModify;
use OwlLabs\OwlMailman\Client\MailmanApi;

/**
 * Class UpdateDraftService
 * @package OwlLabs\OwlMailman\Cli\Service\UpdateDraft
 */
class UpdateDraftService
{
    /**
     * @var FileHandler
     */
    private $fileHandler;

    /**
     * @var MailmanApi
     */
    private $api;

    /**
     * UpdateDraftService constructor.
     * @param FileHandler $fileHandler
     * @param MailmanApi $api
     */
    public function __construct(FileHandler $fileHandler, MailmanApi $api)
    {
        $this->fileHandler = $fileHandler;
        $this->api = $api;
    }

    /**
     * @param string $templateId
     * @param string $filepath
     * @return void
     */
    public function update(string $templateId, string $filepath)
    {
        $contents = $this->fileHandler->loadContents($filepath);

        $input = new TemplateVersionModify($contents);

        $this->api->template($templateId)->version('draft')->modify($input);
    }
}
