<?php
declare(strict_types=1);

namespace OwlLabs\OwlMailman\Cli\Service\CreateTemplate;

use OwlLabs\OwlMailman\Client\Data\Input\TemplateCreate;
use OwlLabs\OwlMailman\Client\MailmanApi;

/**
 * Class CreateTemplateService
 * @package OwlLabs\OwlMailman\Cli\Service\CreateTemplate
 */
class CreateTemplateService
{
    /**
     * @var MailmanApi
     */
    private $api;

    /**
     * CreateTemplateService constructor.
     * @param MailmanApi $api
     */
    public function __construct(MailmanApi $api)
    {
        $this->api = $api;
    }

    /**
     * @param string $name
     * @return string
     */
    public function create(string $name): string
    {
        return $this->api->templates()->create(new TemplateCreate($name));
    }
}
