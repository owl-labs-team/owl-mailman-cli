<?php
declare(strict_types=1);

namespace OwlLabs\OwlMailman\Cli\Service\CreateTemplate;

use OwlLabs\OwlMailman\Client\MailmanApi;
use Psr\Container\ContainerInterface;

/**
 * Class CreateTemplateFactory
 * @package OwlLabs\OwlMailman\Cli\Service\CreateTemplate
 */
class CreateTemplateFactory
{
    /**
     * @param ContainerInterface $container
     * @return CreateTemplateService
     */
    public function __invoke(ContainerInterface $container): CreateTemplateService
    {
        return new CreateTemplateService(
            $container->get(MailmanApi::class)
        );
    }
}
