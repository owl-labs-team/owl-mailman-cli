<?php
declare(strict_types=1);

namespace OwlLabs\OwlMailman\Cli\Command\Templates;

/**
 * Class PreviewException
 * @package OwlLabs\OwlMailman\Cli\Command\Templates
 */
class PreviewException extends \RuntimeException
{
}
