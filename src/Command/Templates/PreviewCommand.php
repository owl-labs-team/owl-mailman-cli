<?php
declare(strict_types=1);

namespace OwlLabs\OwlMailman\Cli\Command\Templates;

use OwlLabs\OwlMailman\Cli\Service\PreviewTemplate\PreviewTemplateService;
use Symfony\Component\Console;

/**
 * Class PreviewCommand
 * @package Console\Command\Data
 */
class PreviewCommand extends Console\Command\Command
{
    /**
     * @var PreviewTemplateService
     */
    private $service;

    /**
     * PreviewCommand constructor.
     * @param PreviewTemplateService $service
     */
    public function __construct(PreviewTemplateService $service)
    {
        parent::__construct();
        $this->service = $service;
    }

    protected function configure()
    {
        $this->setName('templates:preview');
        $this->setDescription('Generates a preview of template with given variables');
        $this->addArgument('template-id', Console\Input\InputArgument::REQUIRED, 'The template ID');
        $this->addOption('use-draft', null, Console\Input\InputOption::VALUE_NONE, 'If present, use draft version');
        $this->addOption(
            'vars-file',
            null,
            Console\Input\InputOption::VALUE_REQUIRED,
            'A path to a JSON file with variables'
        );
        $this->addOption(
            'vars-option',
            null,
            Console\Input\InputOption::VALUE_REQUIRED,
            'An option with JSON-formatted variables'
        );
        $this->addOption(
            'vars-input',
            null,
            Console\Input\InputOption::VALUE_NONE,
            'If present, will ask for variables on runtime'
        );
    }

    /**
     * @param Console\Input\InputInterface $input
     * @param Console\Output\OutputInterface $output
     * @return int
     */
    protected function execute(Console\Input\InputInterface $input, Console\Output\OutputInterface $output)
    {
        $variables = $this->getVariables($input, $output);
        $html = $this->service->preview(
            $input->getArgument('template-id'),
            $input->getOption('use-draft') ? 'draft' : 'public',
            $variables
        );

        $output->writeln($html);

        return 0;
    }

    /**
     * @param Console\Input\InputInterface $input
     * @param Console\Output\OutputInterface $output
     * @return array
     */
    private function getVariables(Console\Input\InputInterface $input, Console\Output\OutputInterface $output): array
    {
        if (($filepath = $input->getOption('vars-file'))) {
            return $this->getVariablesFromFile($filepath);
        }

        if (($option = $input->getOption('vars-option'))) {
            return $this->decodeJson($option);
        }

        if ($input->getOption('vars-input')) {
            $sfInput = new Console\Style\SymfonyStyle($input, $output);
            return $this->decodeJson($sfInput->ask('Insert JSON with variables'));
        }

        throw new PreviewException('One of the options is required: vars-file, vars-option, vars-input');
    }

    /**
     * @param string $filepath
     * @return array
     * @throws PreviewException
     */
    private function getVariablesFromFile(string $filepath): array
    {
        if (!is_readable($filepath)) {
            throw new PreviewException('Could not open the file');
        }
        return $this->decodeJson(file_get_contents($filepath));
    }

    /**
     * @param string $json
     * @return array
     * @throws PreviewException
     */
    private function decodeJson(string $json): array
    {
        $data = json_decode($json, true);

        if (json_last_error() !== JSON_ERROR_NONE) {
            throw new PreviewException(json_last_error_msg(), json_last_error());
        }

        return $data;
    }
}
