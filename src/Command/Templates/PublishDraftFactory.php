<?php
declare(strict_types=1);

namespace OwlLabs\OwlMailman\Cli\Command\Templates;

use OwlLabs\OwlMailman\Cli\Service\PublishDraft\PublishDraftService;
use Psr\Container\ContainerInterface;

/**
 * Class PublishDraftFactory
 * @package OwlLabs\OwlMailman\Cli\Command\Templates
 */
class PublishDraftFactory
{
    /**
     * @param ContainerInterface $container
     * @return PublishDraftCommand
     */
    public function __invoke(ContainerInterface $container): PublishDraftCommand
    {
        return new PublishDraftCommand(
            $container->get(PublishDraftService::class)
        );
    }
}
