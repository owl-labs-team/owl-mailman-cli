<?php
declare(strict_types=1);

namespace OwlLabs\OwlMailman\Cli\Command\Templates;

use OwlLabs\OwlMailman\Cli\Service\PublishDraft\PublishDraftService;
use Symfony\Component\Console;

/**
 * Class PublishDraftCommand
 * @package Console\Command\Data
 */
class PublishDraftCommand extends Console\Command\Command
{
    /**
     * @var PublishDraftService
     */
    private $service;

    /**
     * PublishDraftCommand constructor.
     * @param PublishDraftService $service
     */
    public function __construct(PublishDraftService $service)
    {
        parent::__construct();
        $this->service = $service;
    }

    protected function configure()
    {
        $this->setName('templates:publish-draft');
        $this->setDescription('Copy the draft version of a template as the public version');
        $this->addArgument('template-id', Console\Input\InputArgument::REQUIRED, 'The template ID');
    }

    /**
     * @param Console\Input\InputInterface $input
     * @param Console\Output\OutputInterface $output
     * @return int
     */
    protected function execute(Console\Input\InputInterface $input, Console\Output\OutputInterface $output)
    {
        $this->service->publish($input->getArgument('template-id'));

        $output->writeln('Done.');

        return 0;
    }
}
