<?php
declare(strict_types=1);

namespace OwlLabs\OwlMailman\Cli\Command\Templates;

use OwlLabs\OwlMailman\Cli\Service\UpdateDraft\UpdateDraftService;
use Symfony\Component\Console;

/**
 * Class UpdateDraftCommand
 * @package Console\Command\Data
 */
class UpdateDraftCommand extends Console\Command\Command
{
    /**
     * @var UpdateDraftService
     */
    private $service;

    /**
     * UpdateDraftCommand constructor.
     * @param UpdateDraftService $service
     */
    public function __construct(UpdateDraftService $service)
    {
        parent::__construct();
        $this->service = $service;
    }

    protected function configure()
    {
        $this->setName('templates:update-draft');
        $this->setDescription('Updates draft contents from a file');
        $this->addArgument('template-id', Console\Input\InputArgument::REQUIRED, 'The template ID');
        $this->addArgument('filepath', Console\Input\InputArgument::REQUIRED, 'File path with the template contents');
    }

    /**
     * @param Console\Input\InputInterface $input
     * @param Console\Output\OutputInterface $output
     * @return int
     */
    protected function execute(Console\Input\InputInterface $input, Console\Output\OutputInterface $output)
    {
        $this->service->update(
            $input->getArgument('template-id'),
            $input->getArgument('filepath')
        );

        $output->writeln('Done.');

        return 0;
    }
}
