<?php
declare(strict_types=1);

namespace OwlLabs\OwlMailman\Cli\Command\Templates;

use OwlLabs\OwlMailman\Cli\Service\UpdateDraft\UpdateDraftService;
use Psr\Container\ContainerInterface;

/**
 * Class UpdateDraftFactory
 * @package OwlLabs\OwlMailman\Cli\Command\Templates
 */
class UpdateDraftFactory
{
    /**
     * @param ContainerInterface $container
     * @return UpdateDraftCommand
     */
    public function __invoke(ContainerInterface $container): UpdateDraftCommand
    {
        return new UpdateDraftCommand(
            $container->get(UpdateDraftService::class)
        );
    }
}
