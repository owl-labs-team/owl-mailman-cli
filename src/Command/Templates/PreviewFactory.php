<?php
declare(strict_types=1);

namespace OwlLabs\OwlMailman\Cli\Command\Templates;

use OwlLabs\OwlMailman\Cli\Service\PreviewTemplate\PreviewTemplateService;
use Psr\Container\ContainerInterface;

/**
 * Class PreviewFactory
 * @package OwlLabs\OwlMailman\Cli\Command\Templates
 */
class PreviewFactory
{
    /**
     * @param ContainerInterface $container
     * @return PreviewCommand
     */
    public function __invoke(ContainerInterface $container): PreviewCommand
    {
        return new PreviewCommand(
            $container->get(PreviewTemplateService::class)
        );
    }
}
