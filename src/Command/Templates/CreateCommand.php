<?php
declare(strict_types=1);

namespace OwlLabs\OwlMailman\Cli\Command\Templates;

use OwlLabs\OwlMailman\Cli\Service\CreateTemplate\CreateTemplateService;
use Symfony\Component\Console;

/**
 * Class CreateCommand
 * @package Console\Command\Data
 */
class CreateCommand extends Console\Command\Command
{
    /**
     * @var CreateTemplateService
     */
    private $service;

    /**
     * CreateCommand constructor.
     * @param CreateTemplateService $service
     * @internal param MailmanApi $api
     */
    public function __construct(CreateTemplateService $service)
    {
        parent::__construct();
        $this->service = $service;
    }

    protected function configure()
    {
        $this->setName('templates:create');
        $this->setDescription('Create new template');
        $this->addOption('name', null, Console\Input\InputOption::VALUE_REQUIRED, 'The name of the template');
    }

    /**
     * @param Console\Input\InputInterface $input
     * @param Console\Output\OutputInterface $output
     * @return int
     */
    protected function execute(Console\Input\InputInterface $input, Console\Output\OutputInterface $output)
    {
        $sfInput = new Console\Style\SymfonyStyle($input, $output);

        $name = $input->getOption('name') ?: $sfInput->ask('Name of the template');

        $templateId = $this->service->create($name);

        $output->writeln(sprintf('Template created (ID %s)', $templateId));

        return 0;
    }
}
