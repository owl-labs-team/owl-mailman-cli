<?php
declare(strict_types=1);

namespace OwlLabs\OwlMailman\Cli\Command\Templates;

use OwlLabs\OwlMailman\Cli\Service\CreateTemplate\CreateTemplateService;
use Psr\Container\ContainerInterface;

/**
 * Class CreateFactory
 * @package OwlLabs\OwlMailman\Cli\Command\Templates
 */
class CreateFactory
{
    /**
     * @param ContainerInterface $container
     * @return CreateCommand
     */
    public function __invoke(ContainerInterface $container): CreateCommand
    {
        return new CreateCommand(
            $container->get(CreateTemplateService::class)
        );
    }
}
