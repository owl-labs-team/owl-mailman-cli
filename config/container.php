<?php
declare(strict_types=1);

use Aura\Di\ContainerBuilder;

// Load configuration
$config = require __DIR__ . '/config.php';

// Build container
$builder = new ContainerBuilder();
return $builder->newConfiguredInstance([
    new OwlLabs\OwlMailman\Cli\ContainerConfiguration($config),
]);
