<?php
declare(strict_types=1);

use Zend\ConfigAggregator\ConfigAggregator;
use Zend\ConfigAggregator\PhpFileProvider;

$aggregator = new ConfigAggregator([
    new PhpFileProvider(__DIR__ . '/autoload/{{,*.}global,{,*.}local}.php'),
    new PhpFileProvider(__DIR__ . '/development.config.php'),
]);

return $aggregator->getMergedConfig();
