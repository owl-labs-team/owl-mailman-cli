<?php
declare(strict_types=1);

return [
    'dependencies' => [
        'factories' => [
            OwlLabs\OwlMailman\Cli\Service\CreateTemplate\CreateTemplateService::class =>
                OwlLabs\OwlMailman\Cli\Service\CreateTemplate\CreateTemplateFactory::class,
            OwlLabs\OwlMailman\Cli\Service\PreviewTemplate\PreviewTemplateService::class =>
                OwlLabs\OwlMailman\Cli\Service\PreviewTemplate\PreviewTemplateFactory::class,
            OwlLabs\OwlMailman\Cli\Service\PublishDraft\PublishDraftService::class =>
                OwlLabs\OwlMailman\Cli\Service\PublishDraft\PublishDraftFactory::class,
            OwlLabs\OwlMailman\Cli\Service\UpdateDraft\UpdateDraftService::class =>
                OwlLabs\OwlMailman\Cli\Service\UpdateDraft\UpdateDraftFactory::class,
        ],
    ],
];
