<?php
declare(strict_types=1);

return [
    'dependencies' => [
        'factories' => [
            OwlLabs\OwlMailman\Client\MailmanApi::class => OwlLabs\OwlMailman\Cli\Container\MailmanApiFactory::class,
        ],
    ],
];
