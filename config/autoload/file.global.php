<?php
declare(strict_types=1);

return [
    'dependencies' => [
        'invokables' => [
            OwlLabs\OwlMailman\Cli\Infrastructure\FileHandler::class =>
                OwlLabs\OwlMailman\Cli\Infrastructure\FileHandler::class
        ],
    ],
];
