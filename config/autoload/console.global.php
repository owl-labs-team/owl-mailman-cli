<?php
declare(strict_types=1);

use OwlLabs\OwlMailman\Cli\Command;

return [
    'console' => [
        'commands' => [
            Command\Templates\CreateCommand::class,
            Command\Templates\PreviewCommand::class,
            Command\Templates\PublishDraftCommand::class,
            Command\Templates\UpdateDraftCommand::class,
        ],
    ],
    'dependencies' => [
        'factories' => [
            Command\Templates\CreateCommand::class => Command\Templates\CreateFactory::class,
            Command\Templates\PreviewCommand::class => Command\Templates\PreviewFactory::class,
            Command\Templates\PublishDraftCommand::class => Command\Templates\PublishDraftFactory::class,
            Command\Templates\UpdateDraftCommand::class => Command\Templates\UpdateDraftFactory::class,
        ],
    ],
];
